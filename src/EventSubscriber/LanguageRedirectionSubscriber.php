<?php

namespace Drupal\language_switcher_browser\EventSubscriber;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\language\LanguageNegotiator;
use Drupal\language\Plugin\LanguageNegotiation\LanguageNegotiationBrowser;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Language redirection event subscriber.
 */
class LanguageRedirectionSubscriber implements EventSubscriberInterface {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The language negotiator.
   *
   * @var \Drupal\language\LanguageNegotiator
   */
  protected $languageNegotiator;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new LanguageRedirectionSubscriber object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\language\LanguageNegotiator $language_negotiator
   *   The language negotiator.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The requests stack.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   */
  public function __construct(
    LanguageManagerInterface $language_manager,
    LanguageNegotiator $language_negotiator,
    RouteMatchInterface $route_match,
    RequestStack $request_stack,
  ) {
    $this->languageManager = $language_manager;
    $this->languageNegotiator = $language_negotiator;
    $this->routeMatch = $route_match;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['onRequest', 30];
    return $events;
  }

  /**
   * Handles the language redirection logic.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The request event.
   */
  public function onRequest(RequestEvent $event) {
    $request = $event->getRequest();

    // Check if the redirection cookie or session variable exists.
    $language_redirect = $request->cookies->get('language_redirect');
    if (!isset($_SESSION['language_redirect']) && !$language_redirect) {

      // Get the preferred browser language.
      $negotiator = $this->languageNegotiator->getNegotiationMethodInstance(LanguageNegotiationBrowser::METHOD_ID);
      $preferred_language = $negotiator->getLangcode($request);

      // Check if the preferred language is different from the current language.
      if ($preferred_language !== $this->languageManager->getCurrentLanguage()->getId()) {
        // Get the current entity.
        $entity = $this->routeMatch->getParameter('node');

        // Check if the entity has translations.
        if ($entity && $entity->getEntityTypeId() === 'node' &&  $entity->hasTranslation($preferred_language)) {
          // Get the translated node.
          $translated_node = $entity->getTranslation($preferred_language);

          // Generate the URL of the translated node.
          $url = $translated_node->toUrl()->toString();

          // Set the redirection cookie or session variable.
          $response = new TrustedRedirectResponse($url);
          $response->headers->setCookie(new Cookie('language_redirect', $preferred_language));
          $_SESSION['language_redirect'] = $preferred_language;

          // Redirect to the translated node.
          $event->setResponse($response);
        }
      }
    }
  }

}
